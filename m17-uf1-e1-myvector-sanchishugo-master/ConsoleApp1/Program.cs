﻿using System;
namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            menu();
        }

        public static void menu()
        {
            var opcio = 1;
            string a = "Guest";
            do
            {
                Console.WriteLine($"User:  {a}");
                Console.WriteLine($"Escull una opció :");
                Console.WriteLine("1. Benvinguda + Canvi de nom");
                Console.WriteLine();
                Console.WriteLine("2. Test de classe MyVector");
                Console.WriteLine();
                Console.WriteLine("3. Test de classe VectorGame");
                Console.WriteLine();
                Console.WriteLine("0. Exit");


                opcio = Convert.ToInt32(Console.ReadLine());

                Console.Clear();
                a = lista(opcio, a);

                Console.Clear();

            } while (opcio != 0);




        }


        public static string lista(int a, string b)
        {

            switch (a)
            {
                case 1: return MuxoTexto();

                case 2: TestMyVector(); return b;

                case 3: TestVectorGame(); return b;
            }
            return b;
        }

        public static string MuxoTexto()
        {
            Console.WriteLine("Introdueix el teu nom");
            var a = Console.ReadLine();
            Console.WriteLine(a + ", Benvingut");
            Console.WriteLine();
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
            return a;

        }


        public static void TestMyVector()
        {
            Console.WriteLine("Inserta dos números per punt (x,y)" + System.Environment.NewLine);
            MyVector.MyVector myVector = new MyVector.MyVector
            {
                myDot1 = new int[] { Convert.ToInt32(Console.ReadLine()), Convert.ToInt32(Console.ReadLine()) },
                myDot2 = new int[] { Convert.ToInt32(Console.ReadLine()), Convert.ToInt32(Console.ReadLine()) }

            };

            Console.WriteLine("Punts que representen el vector:");
            Console.WriteLine(myVector.ToString());

            Console.WriteLine("Inversió del vector:");

            myVector.changeDirec();
            Console.WriteLine(myVector.ToString());

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }

        public static void TestVectorGame()
        {

            Console.WriteLine("Generació de llista de vectors aleatoris");
            var a = new VectorGame.VectorGame();
            var c = a.randomVectors(Convert.ToInt32(Console.ReadLine()));
            foreach (var b in c)
            {
                Console.WriteLine(b.ToString());
            }


            Console.WriteLine("Ordenació de vector");

            Console.WriteLine("1 = ordenar longitut  ||  other = Ordenar distancia desde zero");

            if (Convert.ToInt32(Console.ReadLine()) == 1)
            {
                a.SortVectors(c, true);
            }
            else
            {
                a.SortVectors(c, false);
            }

            foreach (var b in c)
            {
                Console.WriteLine(b.ToString());
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadKey();

        }


    }
}
