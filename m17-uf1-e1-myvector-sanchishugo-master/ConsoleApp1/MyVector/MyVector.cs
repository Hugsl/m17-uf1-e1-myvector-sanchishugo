﻿using System;
using System.Collections.Generic;
using System.Text;
using MathNet;

namespace ConsoleApp1.MyVector
{
    class MyVector
    {
        public int[] myDot1 { get; set; }
        public int[] myDot2 { get; set; }



        // Cambiar el vector 1 pel vector 2 i viceversa
        public void changeDirec()
        {
            // x a aux
            var aux = new int[] { 0, 0 };
            aux[0] = myDot1[0];
            aux[1] = myDot1[1];


            // de y a x

            myDot1[0] = myDot2[0];
            myDot1[1] = myDot2[1];

            // de aux a y

            myDot2[0] = aux[0];
            myDot2[1] = aux[1];
        }



        // llargada de la dis
        public double getDistance()
        {
            var x = myDot2[0] - myDot1[0];
            var y = myDot2[1] - myDot1[1];

            var d = System.Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));

            return d;
        }




        // distancia desde centre

        public double getDistanceOrigen()
        {

            double distancia;



            if (myDot1[0] + myDot1[1] > myDot2[0] + myDot2[1])

            {
                distancia = System.Math.Sqrt(Math.Pow(myDot2[0], 2) + Math.Pow(myDot2[1], 2));

            }
            else
            {
                distancia = System.Math.Sqrt(Math.Pow(myDot1[0], 2) + Math.Pow(myDot1[1], 2));
            }



            return distancia;
        }



        public override string ToString()
        {


            string a = $"Vector 1 = [{myDot1[0]},{myDot1[1]}] Vector 2 = [{myDot2[0]},{myDot2[1]}]      LLargada del Vector = {getDistance()}  || Distancia desde el punt mes proper a 0,0: {getDistanceOrigen()}";

            return a;
        }


    }
}
