﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//u lost the game https://es.wikipedia.org/wiki/El_Juego_(juego_mental)


namespace ConsoleApp1.VectorGame
{
    class VectorGame
    {

        public MyVector.MyVector[] randomVectors(int a)
        {

            List<MyVector.MyVector> list = new List<MyVector.MyVector>();

            Random rand1 = new Random();
            Random rand2 = new Random();

            for (int l = 0; l < a; l++)
            {

                var no = new MyVector.MyVector() { myDot1 = new int[a], myDot2 = new int[a] };

                for (int i = 0; i < 2; i++)
                {
                    for (int y = 0; y < a; y++)
                    {
                        no.myDot1[y] = rand1.Next(0, 21);
                    }

                    for (int y = 0; y < a; y++)
                    {
                        no.myDot2[y] = rand2.Next(0, 21);
                    }

                }
                list.Add(no);
            }

            return list.ToArray();
        }

        public MyVector.MyVector[] SortVectors(MyVector.MyVector[] array, bool opcio)
        {

            /*


            donat un array de MyVectors retorna aquest array segons la distància dels vectors o 
            segons proximitat a l’origen.
            Arriba per paràmetre l’array de MyVectors i un booleà segons el tipus d’ordre.

            true == distància

            false == proximitat



             */



            if (opcio)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    for (int i = 0; i < array.Length; i++)
                    {
                        if (i != array.Length - 1)
                        {
                            while (array[i].getDistanceOrigen() > array[i + 1].getDistanceOrigen())
                            {
                                if (array[i].getDistanceOrigen() > array[i + 1].getDistanceOrigen())
                                {

                                    var aux = array[i];
                                    array[i] = array[i + 1];
                                    array[i + 1] = aux;
                                }

                            }

                        }
                    }
                }


                return array;
            }
            else
            {
                Console.WriteLine(" Ordenant per proximitat");
                for (int j = 0; j < array.Length; j++)
                {
                    for (int i = 0; i < array.Length; i++)
                    {
                        if (i != array.Length - 1)
                        {
                            while (array[i].getDistance() > array[i + 1].getDistance())
                            {
                                if (array[i].getDistance() > array[i + 1].getDistance())
                                {

                                    var aux = array[i];
                                    array[i] = array[i + 1];
                                    array[i + 1] = aux;
                                }

                            }

                        }
                    }
                }



                return array;

            }






        }

    }
}
